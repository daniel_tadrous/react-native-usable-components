import React, { Component } from 'react';
import { Text, StyleSheet, Platform } from 'react-native';

export class TextView extends Component {
    render() {
        return (
            <Text style={[style.font, this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}

const style = StyleSheet.create({
    font: {
        fontWeight: Platform.OS !== 'ios' ? '400' : 'normal',
        fontSize: 14,
        color: 'black'
    }
});
