import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

export class Header extends Component {

    render() {
        return (
            <View style={style.container} >
                {this.props.children}
            </View>
        );
    }
}

const style = StyleSheet.create({
    
    container: {
        backgroundColor: '#fbc768',
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        shadowColor: '#010101',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 10
    }
    
});
