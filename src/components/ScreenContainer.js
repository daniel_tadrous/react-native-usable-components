import React, { Component } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';

export class ScreenContainer extends Component {

    render() {
        return (
            <SafeAreaView style={style.container} >
                {this.props.children}
            </SafeAreaView>
        );
    }

}

const style = StyleSheet.create({
    
    container: {
        backgroundColor: '#faebd7',
        flex: 1,
        height: 40
    }
    
});
